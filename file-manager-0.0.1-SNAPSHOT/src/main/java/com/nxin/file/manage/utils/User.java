/**
 * 
 */
package com.nxin.file.manage.utils;

/**
 * @author qijin
 *
 */
public class User {
	private String no;
	private String name;
	private Integer age;
	private String gender;
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	@Override
	public String toString() {
		return "User [no=" + no + ", name=" + name + ", age=" + age + ", gender=" + gender + "]";
	}
	
	
}
