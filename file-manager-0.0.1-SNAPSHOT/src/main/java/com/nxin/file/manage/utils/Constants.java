/**
 * 
 */
package com.nxin.file.manage.utils;

/**
 * @author qijin
 *
 */
public class Constants {
	/**文本默认编码*/
	public static final String TEXT_DEFAULT_CHARTSET = "UTF-8";
	/**文本默认换行符*/
	public static final String TEXT_DEFAULT_LINE_FEEDS = "\r\n";
	
	
}
