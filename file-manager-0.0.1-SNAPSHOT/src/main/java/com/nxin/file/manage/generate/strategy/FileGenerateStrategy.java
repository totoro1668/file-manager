/**
 * 文档生成策略接口
 */
package com.nxin.file.manage.generate.strategy;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * @author qijin
 *
 */
public interface FileGenerateStrategy {
	/**
	 * 根据数据生成文档
	 * @param data 写入文档的数据
	 * @param out 生成文件的输出流
	 */
	void generate(Object data, File file) throws FileNotFoundException;
}
