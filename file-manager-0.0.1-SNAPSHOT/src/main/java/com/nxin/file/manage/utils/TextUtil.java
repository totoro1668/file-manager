/**
 * 
 */
package com.nxin.file.manage.utils;

/**
 * @author qijin
 *
 */
public class TextUtil {
	public static final String NUMBER_REX = "-?[0-9]+.*[0-9]*";
	
	/**
	 * 判断字符串是否是数字格式
	 * 支持负数和小数格式
	 * @param input
	 * @return
	 */
	public static boolean isNumber(String input) {
		if (null == input) {
			return false;
		}
		return input.matches(NUMBER_REX);
	}
}
