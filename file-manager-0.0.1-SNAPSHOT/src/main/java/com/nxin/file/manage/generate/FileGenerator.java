/**
 * 文档生成器抽象接口
 */
package com.nxin.file.manage.generate;

import java.io.File;
import java.io.FileNotFoundException;

import com.nxin.file.manage.generate.strategy.FileGenerateStrategy;

/**
 * @author qijin
 *
 */
public abstract class FileGenerator {
	/**
	 * 文档生成策略
	 */
	protected FileGenerateStrategy strategy;

	protected FileGenerator() {
	}

	protected FileGenerator(FileGenerateStrategy strategy) {
		this.strategy = strategy;
	}

	public void setStrategy(FileGenerateStrategy strategy) {
		this.strategy = strategy;
	}
	
	/**
	 * 生成文档的方法
	 * @param file 生成的文件
	 * @throws FileNotFoundException
	 */
	public abstract void generate(File file) throws FileNotFoundException;
	
	
	
}
