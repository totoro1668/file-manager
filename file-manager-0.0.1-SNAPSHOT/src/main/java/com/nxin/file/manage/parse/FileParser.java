/**
 * 文件解析器
 */
package com.nxin.file.manage.parse;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.nxin.file.manage.parse.strategy.FileParseStrategy;

import jxl.read.biff.BiffException;

/**
 * @author qijin
 *
 */
public abstract class FileParser {
	protected FileParseStrategy strategy;
	
	public FileParser() {
	}

	protected FileParser(FileParseStrategy strategy) {
		this.strategy = strategy;
	}

	protected void setStrategy(FileParseStrategy strategy) {
		this.strategy = strategy;
	}
	
	/**
	 * 解析文档的方法
	 * @param file 解析的文件
	 * @throws IOException 
	 * @throws BiffException 
	 * @throws FileNotFoundException
	 */
	public abstract void parse(File file) throws BiffException, IOException;
	
	
}
