/**
 * Excel解析器的回调函数
 */
package com.nxin.file.manage.parse.callback;

import jxl.Cell;

/**
 * @author qijin
 *
 */
public abstract class AbstractExcelParseCallback implements ExcelParseCallback {
	/* (non-Javadoc)
	 * @see com.nxin.file.manage.parse.callback.ExcelCallback#preCallRow()
	 */
	public boolean preCallRowOrColumn() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.nxin.file.manage.parse.callback.ExcelCallback#afterCallRow()
	 */
	public boolean afterCallRowOrColumn() {
		return true;
	}

}
