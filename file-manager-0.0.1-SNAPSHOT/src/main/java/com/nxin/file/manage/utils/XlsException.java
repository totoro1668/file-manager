/**
 * 生成xls的异常
 */
package com.nxin.file.manage.utils;

/**
 * @author qijin
 *
 */
public class XlsException extends RuntimeException {

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public XlsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public XlsException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public XlsException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public XlsException(Throwable cause) {
		super(cause);
	}
	
}
