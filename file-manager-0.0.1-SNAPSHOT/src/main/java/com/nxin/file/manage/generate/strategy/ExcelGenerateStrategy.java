/**
 * Excel生成策略
 */
package com.nxin.file.manage.generate.strategy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import com.nxin.file.manage.utils.User;
import com.nxin.file.manage.utils.XlsUtil;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

/**
 * @author qijin
 *
 */
public class ExcelGenerateStrategy implements FileGenerateStrategy {

	public ExcelGenerateStrategy() {
	}

	/* (non-Javadoc)
	 * @see com.nxin.file.manage.generate.strategy.FileGenerateStrategy#generate(java.lang.Object, java.io.OutputStream)
	 */
	public void generate(Object data, File file) throws FileNotFoundException {
		WritableWorkbook workbook = null;
		try {
			// 创建工作簿
			workbook = Workbook.createWorkbook(file);
			// 创建页面
			WritableSheet sheet1 = workbook.createSheet("第一页", 0);
			
			// 输出标题头
			// 注意列之间用","间隔,写完一行需要回车换行
			String title = "序号,姓名,年龄,性别";
			WritableCellFormat format = new WritableCellFormat();
			format.setBackground(Colour.BLUE_GREY);
			format.setAlignment(Alignment.CENTRE);
			format.setBorder(Border.TOP, BorderLineStyle.SLANTED_DASH_DOT);
			WritableFont font = new WritableFont(WritableFont.createFont("楷书"), 14);
			font.setBoldStyle(WritableFont.BOLD);
			font.setColour(Colour.WHITE);
			format.setFont(font);
			XlsUtil.fillStringLabelsForRow(sheet1, 0, 0, title.split(","), format);
			
			// 输出内容
			List<User> userList = (List<User>) data;
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < userList.size(); i++) {
				User user = userList.get(i);
				sb.delete( 0, sb.length() );
				// 注意列之间用","间隔,写完一行需要回车换行
				sb.append(user.getNo()).append(",").append(user.getName()).append(",").append(user.getAge()).append(",").append(user.getGender());
				XlsUtil.fillAutoTransLabelsForRow(sheet1, 0, i+1, sb.toString().split(","),null);
			}
			
			workbook.write();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			try {
				if (null != workbook) {
					workbook.close();
				}
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	private WritableSheet fillStringLabelsForLine(WritableSheet sheet, int colNum, int lineNum, String[] arr) throws RowsExceededException, WriteException {
		WritableCellFormat format = new WritableCellFormat();
		format.setBackground(Colour.RED);
		format.setAlignment(Alignment.RIGHT);
		format.setBorder(Border.TOP, BorderLineStyle.SLANTED_DASH_DOT);
		format.setFont(new WritableFont(WritableFont.createFont("楷书"), 20));
		for(int i=0;i < arr.length; i++) {
			sheet.addCell(new Label(colNum+i,lineNum,arr[i],format));
		}
		return sheet;
	}

}
