package com.nxin.file.manage.parse;

import java.io.File;
import java.io.IOException;

import com.nxin.file.manage.parse.strategy.FileParseStrategy;

import jxl.read.biff.BiffException;

public class ExcelParser extends FileParser{
	public ExcelParser() {
		super();
	}

	/**
	 * @param strategy
	 */
	public ExcelParser(FileParseStrategy strategy) {
		super(strategy);
	}

	@Override
	public void parse(File file) throws BiffException, IOException {
		// 针对文件的一些业务逻辑
		// ...
		// 调用解析策略
		strategy.parse(file);
	}

}
