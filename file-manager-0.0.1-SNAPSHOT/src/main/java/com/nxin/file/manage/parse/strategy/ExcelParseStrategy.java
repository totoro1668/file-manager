/**
 * 
 */
package com.nxin.file.manage.parse.strategy;

import java.io.File;
import java.io.IOException;

import com.nxin.file.manage.parse.callback.AbstractExcelParseCallback;
import com.nxin.file.manage.parse.callback.ExcelParseCallback;
import com.nxin.file.manage.utils.XlsUtil;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 * @author qijin
 *
 */
public class ExcelParseStrategy implements FileParseStrategy {

	/* (non-Javadoc)
	 * @see com.nxin.file.manage.parse.strategy.FileParseStrategy#parse(java.io.InputStream)
	 */
	public Object parse(File file) throws BiffException, IOException {
		// 读取excel文件，获取工作簿
		Workbook workbook = Workbook.getWorkbook(file);
		// 获取第一个工作表
		Sheet sheet = workbook.getSheet(0);
		// 获取第一行标题
		final StringBuilder sb = new StringBuilder();
		XlsUtil.readRow(sheet, 0, new AbstractExcelParseCallback() {
			@Override
			public boolean afterCallRowOrColumn() {
				sb.deleteCharAt(sb.length()-1);
				System.out.println(sb.toString());
				return false;
			}
			
			public boolean callCell(Cell cell) {
				sb.append(cell.getContents()).append(',');
				return true;
			}
		});
		
		// 获取内容
		XlsUtil.readRows(sheet, 1, new ExcelParseCallback() {
			public boolean preCallRowOrColumn() {
				sb.delete(0, sb.length());
				return true;
			}
			
			public boolean callCell(Cell cell) {
				sb.append(cell.getContents()).append(',');
				return true;
			}
			
			public boolean afterCallRowOrColumn() {
				sb.deleteCharAt(sb.length()-1);
				System.out.println(sb.toString());
				return true;
			}
		});
		
		return null;
	}

}
