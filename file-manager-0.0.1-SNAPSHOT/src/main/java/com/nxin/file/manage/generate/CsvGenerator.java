/**
 * 
 */
package com.nxin.file.manage.generate;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import com.nxin.file.manage.generate.strategy.CsvGenerateStrategy;
import com.nxin.file.manage.utils.User;

/**
 * @author qijin
 *
 */
public class CsvGenerator extends FileGenerator {
	public CsvGenerator() {
		super();
	}
	public CsvGenerator(CsvGenerateStrategy csvGenerateStrategy) {
		super(csvGenerateStrategy);
	}

	public void generate(File file) throws FileNotFoundException {
		// 组合业务数据
		List<User> users = new ArrayList<User>();
		for (int i = 0; i < 100; i++) {
			User user = new User();
			user.setNo(String.valueOf(i));
			user.setName("姓名-" + i);
			user.setAge(i);
			if (i < 50) {
				user.setGender("男");
			} else {
				user.setGender("男");
			}
			users.add(user);
		}

		strategy.generate(users, file);

	}
}
