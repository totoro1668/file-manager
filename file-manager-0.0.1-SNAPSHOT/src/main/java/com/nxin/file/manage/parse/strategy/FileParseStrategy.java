/**
 * 文档解析策略接口
 */
package com.nxin.file.manage.parse.strategy;

import java.io.File;
import java.io.IOException;

import jxl.read.biff.BiffException;

/**
 * @author qijin
 *
 */
public interface FileParseStrategy {
	/**
	 * 解析文档
	 * @param in 文档输入流
	 * @return
	 * @throws IOException 
	 * @throws BiffException 
	 */
	Object parse(File file) throws BiffException, IOException;
}
