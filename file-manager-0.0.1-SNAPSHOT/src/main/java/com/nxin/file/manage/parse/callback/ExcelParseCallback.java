/**
 * 
 */
package com.nxin.file.manage.parse.callback;

import jxl.Cell;

/**
 * @author qijin
 *
 */
public interface ExcelParseCallback {
	/**
	 * 读取到单元格内容后调用该方法
	 * @param cell 读取到的单元格数据
	 * @return
	 */
	boolean callCell(Cell cell);
	/**
	 * 读取行或列数据之前调用该方法
	 * @return
	 */
	boolean preCallRowOrColumn();
	/**
	 * 读取行或列数据完成后调用该方法
	 * @return
	 */
	boolean afterCallRowOrColumn();
}
