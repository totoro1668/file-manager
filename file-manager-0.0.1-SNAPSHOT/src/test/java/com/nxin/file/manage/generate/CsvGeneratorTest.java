/**
 * 
 */
package com.nxin.file.manage.generate;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;

import org.junit.Test;

import com.nxin.file.manage.generate.strategy.CsvGenerateStrategy;

/**
 * @author qijin
 *
 */
public class CsvGeneratorTest {

	/**
	 * Test method for {@link com.nxin.file.manage.generate.CsvGenerator#generate(java.lang.String)}.
	 */
	@Test
	public void testGenerate() {
		CsvGenerator generator = new CsvGenerator(new CsvGenerateStrategy());
		try {
			generator.generate(new File("d:/user.csv"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}

}
