/**
 * 
 */
package com.nxin.file.manage.parse;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import com.nxin.file.manage.parse.strategy.ExcelParseStrategy;

import jxl.read.biff.BiffException;

/**
 * @author qijin
 *
 */
public class ExcelParserTest {
	
	@Test
	public void testparse() {
		File file = new File("d:/user.xls");
		ExcelParser parser = new ExcelParser(new ExcelParseStrategy());
		try {
			parser.parse(file);
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
