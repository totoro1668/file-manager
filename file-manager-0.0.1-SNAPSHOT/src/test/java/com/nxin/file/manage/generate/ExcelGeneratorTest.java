/**
 * 
 */
package com.nxin.file.manage.generate;

import java.io.File;
import java.io.FileNotFoundException;

import org.junit.Test;

import com.nxin.file.manage.generate.strategy.ExcelGenerateStrategy;

/**
 * @author qijin
 *
 */
public class ExcelGeneratorTest {

	/**
	 * Test method for {@link com.nxin.file.manage.generate.ExcelGenerator#generate(java.io.File)}.
	 */
	@Test
	public void testGenerate() {
		ExcelGenerator generator = new ExcelGenerator(new ExcelGenerateStrategy());
		try {
			generator.generate(new File("d:/user.xls"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}

}
